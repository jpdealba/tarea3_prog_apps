import 'package:flutter/material.dart';

class Donaciones extends StatefulWidget {
  final donativos;
  Donaciones({Key? key, required this.donativos}) : super(key: key);

  @override
  State<Donaciones> createState() => _DonacionesState();
}

class _DonacionesState extends State<Donaciones> {
  @override
  Widget build(BuildContext context) {
    print(widget.donativos["metaCumplida"]);
    return Scaffold(
        appBar: AppBar(
          title: Text("Donativos Obtenidos"),
        ),
        body: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            children: [
              ListTile(
                leading: Image.asset("assets/paypal.png"),
                trailing: Text(
                  "\$${widget.donativos["paypal"] ?? 0.0}",
                  style: TextStyle(fontSize: 32),
                ),
              ),
              SizedBox(height: 15),
              ListTile(
                leading: Image.asset("assets/tarjeta_de_credito.png"),
                trailing: Text(
                  "\$${widget.donativos["tarjeta"] ?? 0.0}",
                  style: TextStyle(fontSize: 32),
                ),
              ),
              SizedBox(height: 15),
              Divider(),
              ListTile(
                leading: Icon(
                  Icons.attach_money,
                  size: 64,
                ),
                trailing: Text(
                  "\$${widget.donativos["acumulado"] ?? 0.0}",
                  style: TextStyle(fontSize: 32),
                ),
              ),
              if (widget.donativos["metaCumplida"] == true)
                Padding(
                  padding: const EdgeInsets.only(top: 100.0),
                  child: Image.asset("assets/gracias.png"),
                )
            ],
          ),
        ));
  }
}
