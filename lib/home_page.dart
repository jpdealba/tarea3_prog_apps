import 'dart:ui';

import 'package:donativos/donaciones.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedRadio = 0;
  int? dropdownValue;
  int totalPaypal = 0;
  int totalTarjeta = 0;
  bool meta = false;
  double total = 0;
  double porcentaje = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Donaciones"),
      ),
      body: Padding(
        padding: EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Es para una buena causa",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
            Text("Elija modo de donativo", style: TextStyle(fontSize: 15)),
            SizedBox(height: 15),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(width: 1),
                  borderRadius: BorderRadius.circular(10)),
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    ListTile(
                      leading: Image.asset("assets/paypal.png"),
                      title: Text("Paypal"),
                      trailing: Radio(
                        value: 0,
                        groupValue: _selectedRadio,
                        onChanged: (val) {
                          setState(() {
                            _selectedRadio = val as int;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 15),
                    ListTile(
                      leading: Image.asset("assets/tarjeta_de_credito.png"),
                      title: Text("Tarjeta"),
                      trailing: Radio(
                        value: 1,
                        groupValue: _selectedRadio,
                        onChanged: (val) {
                          setState(() {
                            _selectedRadio = val as int;
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Text(
                "Cantidad a donar: ",
                style: TextStyle(fontSize: 17),
              ),
              trailing: DropdownButton<int>(
                underline: Container(
                  height: 0,
                ),
                value: dropdownValue,
                icon: Icon(Icons.arrow_downward_sharp),
                items: [100, 350, 850, 1050, 9999]
                    .map<DropdownMenuItem<int>>((int value) {
                  return DropdownMenuItem<int>(
                    value: value,
                    child: Text('$value'),
                  );
                }).toList(),
                onChanged: (int? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
              ),
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.only(top: 30.0, bottom: 10),
              child: LinearProgressIndicator(
                value: porcentaje,
                backgroundColor: Colors.transparent,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 30.0),
              child: Center(
                child: Text('${(porcentaje * 100).toStringAsFixed(2)}%'),
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: Size.fromHeight(
                    40), // fromHeight use double.infinity as width and 40 is the height
              ),
              onPressed: () {
                setState(() {
                  if (dropdownValue != null) {
                    if (_selectedRadio == 0) {
                      totalPaypal += dropdownValue as int;
                    } else if (_selectedRadio == 1) {
                      totalTarjeta += dropdownValue as int;
                    }
                    total += dropdownValue as int;
                    if (total >= 10000) {
                      meta = true;
                      porcentaje = 1;
                    } else
                      porcentaje = getPercentage(total, dropdownValue, meta);
                  }
                });
              },
              child: Text('Donar'),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: "Ver Donativos",
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => Donaciones(
                donativos: {
                  "paypal": totalPaypal,
                  "tarjeta": totalTarjeta,
                  "acumulado": total,
                  "metaCumplida": meta
                },
              ),
            ),
          );
        },
        child: Icon(Icons.skip_next),
      ),
    );
  }
}

double getPercentage(total, dropdownValue, meta) {
  if ((total + dropdownValue) >= 10000.00) {
    meta = true;
    return 1;
  } else {
    return (((total * 100) / 10000) / 100);
  }
}
